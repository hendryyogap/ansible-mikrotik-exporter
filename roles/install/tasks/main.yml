---

- name: Gather variables for each operating system
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution_file_variety | lower }}.yml"
    - "{{ ansible_distribution | lower }}.yml"
    - "{{ ansible_os_family | lower }}.yml"

- name: Install dependencies
  package:
    name: "{{ item }}"
    state: present
  register: _install_dep_packages
  until: _install_dep_packages is success
  retries: 5
  delay: 2
  with_items: "{{ node_exporter_dependencies }}"

- name: Git clone mikrotik_exporter
  git:
    repo: 'https://github.com/nshttpd/mikrotik-exporter'
    dest: '/tmp/mikrotik-exporter'
    force: yes

- name: Copy BGP collector from template
  template:
    src: bgp_collector.go.j2
    dest: /tmp/mikrotik-exporter/collector/bgp_collector.go
    owner: root
    group: root
    mode: 0644

- name: compile mikrotik exporter source
  command: 'make'
  args:
    chdir: /tmp/mikrotik-exporter
  retries: 5
  delay: 5

- name: build mikrotik exporter source
  command: 'go build'
  args:
    chdir: /tmp/mikrotik-exporter
  delay: 5

- name: Create /usr/local/bin
  file:
    path: /usr/local/bin
    state: directory
    mode: 0755

- name: Propagate mikrotik_exporter binary
  copy:
    src: "/tmp/mikrotik-exporter/mikrotik-exporter"
    dest: "/usr/local/bin/mikrotik_exporter"
    mode: 0755
    owner: root
    group: root
    remote_src: yes

- name: Configure mikrotik_exporter
  template:
    src: mikrotik_exporter.yml.j2
    dest: "{{ mikrotik_exporter_config_file }}"
    mode: 0644

- name: Copy the mikrotik Exporter systemd service file
  template:
    src: mikrotik_exporter.service.j2
    dest: "{{ mikrotik_exporter_systemd_file }}"
    owner: root
    group: root
    mode: 0644
  notify: restart mikrotik_exporter

- name: Start & enable Process Exporter
  systemd:
    name: mikrotik_exporter
    state: started
    enabled: true
    daemon_reload: true
