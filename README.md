Ansible Installer for Mikrotik-exporter
=========

## Description

An installer for Prometheus Exporter for Mikrotik devices. Can be configured to collect metrics from a single device or multiple devices. Single device monitoring can be configured all on the command line. Multiple devices require a configuration file. A user will be required that has read-only access to the device configuration via the API.

## Reference

Please read the readme on https://github.com/nshttpd/mikrotik-exporter

Ansible role to install mikrotik-exporter

Requirements
------------

Install this exporter on the same prometheus server

Role Variables
--------------

```YAML
# process_exporter version to be installed
mikrotik_exporter_version: latest

# process_exporter config file path
mikrotik_exporter_config_file: /etc/mikrotik_exporter.yml

# process_exporter listen address
mikrotik_exporter_web_listen_address: 0.0.0.0:9436

```

## Config File

After install, you have to change the configuration file. Config file, placed in the ``` /etc/mikrotik_exporter.yml ```


Dependencies
------------

None

How to install Playbook
----------------

```
ansible-playbook -i hosts --limit=dockerbuild.daffakidz.com main.yml
```

# NOTE

After install you have to update the configuration ``` /etc/mikrotik_exporter.yml ```
